package base.commandPattern;

import base.Domino;
import base.Location;
import base.Main;

public class SetupADomino {
	private int x, y;
	private String s3;
	private Main main;
	
	public SetupADomino(Main main, int x, int y) {
		this.x = x;
		this.y = y;
		this.main = main;
	}
	
	public void setS3(String s3) {
		this.s3 = s3;
	}
	
	public void placeDomino() {
		int y2 = -9,
        x2 = -9;
		Location lotion;
          if (s3 != null && s3.toUpperCase().startsWith("H")) {
            lotion = new Location(x, y, Location.DIRECTION.HORIZONTAL);
            System.out.println("Direction to place is " + lotion.d);
            x2 = x + 1;
            y2 = y;
          }
          if (s3 != null && s3.toUpperCase().startsWith("V")) {
            lotion = new Location(x, y, Location.DIRECTION.VERTICAL);
            System.out.println("Direction to place is " + lotion.d);
            x2 = x;
            y2 = y + 1;
          }
        if (x2 > 7 || y2 > 6) {
          System.out
              .println("Problems placing the domino with that position and direction");
        } else {
          
          Domino d = main.findGuessByLH(main.grid[y][x], main.grid[y2][x2]);
          if (d == null) {
            System.out.println("There is no such domino");
            return;
          }
          
          if (d.placed) {
            System.out.println("That domino has already been placed :");
            System.out.println(d);
            return;
          }
          
          if (main.gg[y][x] != 9 || main.gg[y2][x2] != 9) {
            System.out.println("Those coordinates are not vacant");
            return;
          }
          
          main.gg[y][x] = main.grid[y][x];
          main.gg[y2][x2] = main.grid[y2][x2];
          if (main.grid[y][x] == d.high && main.grid[y2][x2] == d.low) {
            d.place(x, y, x2, y2);
          } else {
            d.place(x2, y2, x, y);
          }
          main.score += 1000;
          main.collateGuessGrid();
          main.pf.dp.repaint();
        }
        
	}
	
	public void unplaceDomino() {
		Domino lkj = main.findGuessAt(x, y);
        if (lkj == null) {
          System.out.println("Couln't find a domino there");
        } else {
          lkj.placed = false;
          main.gg[lkj.hy][lkj.hx] = 9;
          main.gg[lkj.ly][lkj.lx] = 9;
          main.score -= 1000;
          main.collateGuessGrid();
          main.pf.dp.repaint();
        }
	}
	
	
}
