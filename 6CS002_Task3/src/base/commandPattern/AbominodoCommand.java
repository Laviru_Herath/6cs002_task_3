package base.commandPattern;

public interface AbominodoCommand {
	void execute();
}
