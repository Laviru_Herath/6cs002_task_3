package base.commandPattern;

public class AbominodoInvoker {
	private AbominodoCommand doCommand, undoCommand;
	public AbominodoInvoker(PlaceADominoCommand doCommand) {
		this.doCommand = doCommand;
	}
	
	public AbominodoInvoker(PlaceADominoCommand doCommand, RemoveADominoCommand undoCommand) {
		this.doCommand = doCommand;
		this.undoCommand = undoCommand;
	}
	
	public void setUndoInvoker(RemoveADominoCommand undoCommand) {
		this.undoCommand = undoCommand;
	}
	
	public void doPlace() {
		doCommand.execute();
	}
	
	public void undoPlace() {
		undoCommand.execute();
	}

}
