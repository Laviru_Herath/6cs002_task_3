package base.commandPattern;

public class PlaceADominoCommand implements AbominodoCommand{
	private SetupADomino doplacedomino;
	
	public PlaceADominoCommand(SetupADomino placedomino) {
		this.doplacedomino = placedomino;
	}

	@Override
	public void execute() {
		doplacedomino.placeDomino();
	}
}
