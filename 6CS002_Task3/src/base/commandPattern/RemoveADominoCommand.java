package base.commandPattern;

public class RemoveADominoCommand implements AbominodoCommand{
	private SetupADomino undoplacedomino;
	
	public RemoveADominoCommand(SetupADomino placedomino) {
		this.undoplacedomino = placedomino;
	}

	@Override
	public void execute() {
		undoplacedomino.unplaceDomino();
	}

}
